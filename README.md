# Duocode - Web

DuoCode é uma plataforma de aprendizado de linguagens de programação inspirado na 
plataforma Duolingo. Mediante métodos de repetição, o aluno percorrerá uma árvore 
de tópicos, resolvendo sessões pequenas de perguntas relacionadas à linguagem, 
não só aprendendo novos conteúdos como revisando conteúdos de módulos passados.

[Acesse a wiki do projeto](https://gitlab.com/projeto-de-desenvolvimento/duo-code/api/-/wikis/home)

### Requisitos

- NodeJS
- Gerenciador de pacote instalado. De prefêrencia, "npm" ou "yarn".
- Git

### Instalação

1. Clone o repositório
1. Faça a instalação dos pacotes necessários para rodar o cliente web, com o comando: 
    * "npm install"
    * ou "yarn install"
1. Copie o arquivo **.env-example** e renomeie para **.env**. Adicione a url da api. Se a api estiver rodando localmente, não será necessário modificações.
1. Execute o cliente: npm start:develop
1. Endereço: localhost:3000/login
1. Faça o seu registro e login
