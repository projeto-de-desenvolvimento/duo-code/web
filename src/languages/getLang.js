import general_pt from "../languages/general_pt.json";
import general_en from "../languages/general_en.json";

export default function getLang(lang) {
    if (lang == "pt-br") {
        return general_pt;
    } else if (lang == "en") {
        return general_en;
    }
}