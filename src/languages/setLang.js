import general_pt from "../languages/general_pt.json";
import general_en from "../languages/general_en.json";

export default function setLang(lang) {
    if (lang == "pt-br") {
        window.localStorage.setItem("language", "pt-br")
        return general_pt;
    } else if (lang = "en") {
        window.localStorage.setItem("language", "en")
        return general_en;
    }
}