import React, { Component } from "react";
import { Link } from "react-router-dom";

import logo from "../icons/logo.png";
import profileImage from "../icons/person.png";
import communityIcon from "../icons/globe.png";
import notificationIcon from "../icons/mail.png";
import offensiveIcon from "../icons/fire.png";
import rankingIcon from "../icons/ranking.png";
import Footer from "../components/Footer";
import API from "../api";

export default class HeaderMenu extends Component {
  constructor(props) {
    super(props);

    this.state = {
      offensive: "-",
      registeredCourses: null,
    };
  }

  componentDidMount = () => {
    this.getUserStats();
  };

  getUserStats() {
    API.get("/user/stats")
      .then((response) => {
        const data = response.data;
        this.setState({
          offensive: data.offensive,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-2 leftContainerLogo d-none d-lg-inline">
            <Link to="/">
              <img src={logo} alt="logo" id="logo" />
            </Link>
            <h2 id="appName">DuoCode</h2>
          </div>
          <div className="col-12 col-lg-10">
            <div className="topMenu d-flex">
              <div className="col-4 d-flex align-items-center">
                <div className="offensive-icon">
                  <img
                    src={offensiveIcon}
                    alt="Ofensiva"
                    className="offensive-icon"
                  />
                </div>
                <div className="topMenuIcon">
                  <span className="offensive-number">
                    {this.state.offensive}
                  </span>
                </div>
              </div>
              <div className="col-8 d-flex justify-content-end align-items-center">
                <div className="topMenuIcon">
                  <Link to="/ranking">
                    <img
                      src={rankingIcon}
                      alt="Ranking"
                      className="topMenuIcon"
                    />
                  </Link>
                </div>
                <div className="topMenuIcon">
                  <Link to="/community">
                    <img
                      src={communityIcon}
                      alt="Comunidade"
                      className="topMenuIcon"
                    />
                  </Link>
                </div>
                <div class="dropdown">
                  <button
                    class="btn dropdown-toggle"
                    type="button"
                    id="dropdownMenuButton"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    <img
                      src={profileImage}
                      alt="Perfil"
                      className="profileImage"
                    />
                  </button>
                  <div
                    class="dropdown-menu"
                    aria-labelledby="dropdownMenuButton"
                  >
                    <a class="dropdown-item" href="/profile">
                      Perfil
                    </a>
                    <a class="dropdown-item" href="/login">
                      Sair
                    </a>
                  </div>
                </div>
              </div>
            </div>
            {this.props.children}
          </div>
        </div>
        <div className="row">
          <div className="col-12">
            <Footer />
          </div>
        </div>
      </div>
    );
  }
}
