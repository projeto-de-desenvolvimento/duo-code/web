import React, { useEffect } from "react";
import API from "../api";
import "../styles/coursePage.css";
import TagsComponent from "../components/community/tags";
import "../styles/community/questionPage.css";
import { Button } from "reactstrap";
import profileImage from "../icons/person.png";
import { useForm } from "react-hook-form";
import likeDislike from "../icons/community/likeDislike.png";

export default function QuestionPage(props) {
  const { register, handleSubmit, watch, errors } = useForm();
  const [question, setQuestion] = React.useState({});

  const onSubmit = async (data, e) => {
    e.target.reset();
    const id = props.match.params.id;

    API.post("/comment", {
      content: data.comment,
      askingId: id,
    })
      .then((response) => {
        getQuestion();
        register = null;
      })
      .catch((error) => {
        console.log(error);
      });
  };

  useEffect(() => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    getQuestion();
  }, []);

  const getQuestion = async () => {
    const id = await props.match.params.id;
    API.get(`asking/${id}`)
      .then((response) => {
        if (response.status === 200) {
          setQuestion(response.data);
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const evaluate = (isLike, commentId) => {
    API.post("reviewRating", { like: isLike, commentId }).then(
      async (response) => {
        const updateQuestion = question;

        const updateIndex = await updateQuestion.comments.findIndex(
          (comment) => comment.id == commentId
        );

        updateQuestion.comments[updateIndex].reviewRatings = {
          positive: response.data.rating.like,
          negative: response.data.rating.dislike,
        };

        setQuestion({ ...updateQuestion });
      }
    );
  };

  return (
    <div className="question-page row col-12">
      <div className="col-12 text-center mt-5">
        <h1>{question.title}</h1>
      </div>
      <div className="my-3">
        <TagsComponent tags={question.tags} />
      </div>
      <div className="col-12 d-flex justify-content-center mt-4">
        <div className="question-page-content col-10">
          <p>{question.content}</p>
        </div>
      </div>

      <div className="comment-section col-12 mt-3 d-flex justify-content-center">
        <form onSubmit={handleSubmit(onSubmit)}>
          <div>
            <div className="d-inline-block">
              <h5>Comentar:</h5>
            </div>
            <div className="d-inline-block mb-2" style={{ float: "right" }}>
              <Button color="info">Comentar</Button>
            </div>
          </div>
          <textarea
            type="textarea"
            name="comment"
            id="comment"
            placeholder=" Comentar"
            ref={register}
          />
        </form>
      </div>

      <div className="row col-12 all-comments-section">
        {question.comments
          ? question.comments.map((comment) => (
              <div className="col-12 mt-2 mb-2 d-flex justify-content-center">
                <div className="">
                  <img
                    src={likeDislike}
                    onClick={() => evaluate(true, comment.id)}
                    style={{
                      width: "20px",
                      transform: "scaleX(-1)",
                      transform: "scaleY(-1)",
                    }}
                  />
                  <p style={{ margin: "0px" }}>
                    {comment.reviewRatings.positive}
                  </p>
                  <img
                    src={likeDislike}
                    onClick={() => evaluate(false, comment.id)}
                    style={{ width: "20px" }}
                  />
                  <p style={{ margin: "0px" }}>
                    {comment.reviewRatings.negative}
                  </p>
                </div>
                <div className="comment-date">
                  <p>{new Date(comment.createdAt).toLocaleDateString()}</p>
                </div>
                <div className="col-2 text-center">
                  <img src={profileImage} alt="imagem de perfil" />
                  <h6>{comment.user.username}</h6>
                </div>
                <div className="comment col-10">
                  <p>{comment.content}</p>
                </div>
              </div>
            ))
          : ""}
      </div>
    </div>
  );
}
