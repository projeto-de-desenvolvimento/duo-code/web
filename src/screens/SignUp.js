import React, { Component } from "react";
import "../styles/signup.css";
import API from "../api";
import { Button } from "reactstrap";
import ErrorMessage from "../components/auth/errorMessage";
import logo from "../icons/logo.png";
import brFlagIcon from "../icons/br-flag.png";
import usFlagIcon from "../icons/us-flag.png";
import getLang from "../languages/getLang";
import setLang from "../languages/setLang";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      email: "",
      password: "",
      passwordRepeat: "",
      aviso: "",
      error: null,
      language_selected: getLang("pt-br")
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount = async () => {
    const lang = window.localStorage.getItem("language");

    if (lang) {
      const file = getLang(lang);
      this.setState({ language_selected: file });
    }
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const errors = await this.validation();

    if (errors.length > 0) {
      this.setState({ error: errors });
      return;
    }

    API.post("/auth/register", {
      username: this.state.username,
      email: this.state.email,
      password: this.state.password,
    })
      .then((response) => {
        if (response) {
          if (response.status === 200) {
            this.setState({
              aviso: this.state.language_selected.signUp["User created successfully. Redirecting"],
            });
            this.tempoAviso();
            setTimeout(() => {
              this.props.history.push("/login");
              window.location.reload();
            }, 2000);
          }
        }
      })
      .catch((error) => {
        if (!error.response) {
          this.setState({ error: [this.state.language_selected.signUp["There was an error with the server"]] });
        }

        let response = error.response;
        if (response?.status === 400) {
          let error = "";
          if (response.data.errors) {
            error = response.data.errors;
            this.setState({ error });
          } else {
            this.setState({ error: [this.state.language_selected.signUp["There was an error with the server"]] });
          }
        }
      });
  };

  validation = async () => {
    let errors = [];
    let username = this.state.username;
    let email = this.state.email;
    let password = this.state.password;
    let passwordRepeat = this.state.passwordRepeat;
    let emailRegex = new RegExp("^[a-zA-Z][a-zA-Z0-9]+@[a-zA-Z]+.[a-zA-Z]+$");

    if (username.length < 5) {
      errors.push("O nome de usuário deve ser maior que 5 caracteres.");
    } else if (username.length > 30) {
      errors.push("O nome de usuário deve conter menos que 30 caracteres.");
    }

    if (!emailRegex.test(email)) {
      errors.push("Você deve digitar um e-mail válido.");
    }

    if (password.length === 0) {
      errors.push("Você deve digitar uma senha.");
    } else if (passwordRepeat.length === 0) {
      errors.push("Você deve repetir a senha.");
    } else if (password.length < 6) {
      errors.push("A senha deve conter no mínimo 6 caracteres.");
    } else if (password !== passwordRepeat) {
      errors.push("As senhas são diferentes.");
    }

    return errors;
  };

  tempoAviso = () => {
    setTimeout(() => {
      this.setState({ aviso: "" });
    }, 1000 * 40);
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.handleSubmit(event);
    }
  };

  render() {
    return (
      <div className="container" onKeyUp={this.handleKeyPress}>
        <div className="row register-panel">
          <div className="background-image"></div>
          <div className="col-sm-12 text-center">
            <img src={logo} alt="logo" id="logoImage" />
            <h3>DuoCode</h3>
            <div className="row d-flex justify-content-center">
              <div className="col-sm-8">
                <div className="row d-flex justify-content-center px-3">
                  <div className="col-sm-8" id="border">
                    <div id="form">
                      <label for="username">
                        {this.state.language_selected.signUp.Username}
                        <br />
                        <input
                          type="text"
                          id="username"
                          name="username"
                          value={this.state.username}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                      <br />
                      <label for="email">
                        {this.state.language_selected.signUp.Mail}
                        <br />
                        <input
                          type="text"
                          id="email"
                          name="email"
                          value={this.state.email}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                      <br />
                      <label for="password">
                        {this.state.language_selected.signUp.Password}
                        <br />
                        <input
                          type="password"
                          id="password"
                          name="password"
                          value={this.state.password}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                      <br />
                      <label for="passwordRepeat">
                        {this.state.language_selected.signUp["Confirm password"]}
                        <br />
                        <input
                          type="password"
                          id="passwordRepeat"
                          name="passwordRepeat"
                          value={this.state.passwordRepeat}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-sm-12">
                <div style={{ textAlign: "center", marginTop: "1vh" }}>
                  <span>
                    {this.state.language_selected.signUp["Already have an account?"]} <a href="/login"> {this.state.language_selected.signUp["Come in!"]}</a>
                  </span>
                </div>
                <form onSubmit={this.handleSubmit} className="">
                  <div style={{ textAlign: "center", marginTop: "3vh" }}>
                    <Button type="submit" color="success">
                      {this.state.language_selected.signUp.Register}
                    </Button>
                  </div>
                </form>
              </div>
              <div>
                {this.state.aviso !== "" ? (
                  <div className="alert-success mt-3">{this.state.aviso}</div>
                ) : (
                    ""
                  )}
                {this.state.error ? (
                  <ErrorMessage error={this.state.error} />
                ) : (
                    ""
                  )}
              </div>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-sm-12 d-flex justify-content-center" id="login-footer">
            <div>
              <Button color="link" onClick={() => this.setState({ language_selected: setLang("pt-br") })}><img src={brFlagIcon} alt="bandeira" style={{ width: "50px" }} /></Button>
              <Button color="link" onClick={() => this.setState({ language_selected: setLang("en") })}><img src={usFlagIcon} alt="bandeira" style={{ width: "45px" }} /></Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default SignUp;
