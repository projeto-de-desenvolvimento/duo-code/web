import React, { useEffect } from "react";
import { Button, Input } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import questionIcon from "../icons/community/question.png";
import { useForm } from "react-hook-form";
import ErrorMessage from "../components/auth/errorMessage";

export default function NewQuestion() {
  const { register, handleSubmit, watch, errors } = useForm();
  const [error, setError] = React.useState([]);

  const onSubmit = (data) => {
    const tags = data.tags.replace(/ /g, "");
    API.post("/asking", {
      title: data.title,
      content: data.content,
      tags,
    })
      .then((response) => {
        alert("Pergunta criada com sucesso!");
      })
      .catch((error) => {
        setError(["Ocorreu um erro com o servidor..."]);
      });
  };

  useEffect(() => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
  }, []);

  return (
    <div className="row mt-4 question-form-panel">
      <div className="col-12 mb-5 mt-2">
        <div className="z-index-2 d-flex justify-content-center align-items-center">
          <img
            src={questionIcon}
            style={{ width: "80px", marginRight: "2vw" }}
          />
          <h3>Nova Pergunta</h3>
        </div>
      </div>

      <div className="col-12 d-flex justify-content-center">
        <div className="col-6">
          {error.length ? <ErrorMessage error={error} /> : ""}
        </div>
      </div>

      <div className="row col-12 d-flex justify-content-center align-items-center new-question-form">
        <div>
          <form onSubmit={handleSubmit(onSubmit)}>
            <div className="col-12 d-flex justify-content-center">
              <input
                type="text"
                id="title"
                name="title"
                placeholder="Título"
                ref={register}
              />
            </div>
            <div className="text-center">
              <p style={{ color: "gray", fontSize: "12px" }}>
                Digite as tags entre ponto e vírgula. Ex: javascript;html;css
              </p>
            </div>
            <div className="col-12 d-flex justify-content-center">
              <input
                type="textarea"
                id="tags"
                name="tags"
                placeholder="Tags"
                ref={register}
              />
            </div>
            <div className="col-12 d-flex justify-content-center">
              <textarea
                type="textarea"
                id="content"
                name="content"
                placeholder="Pergunta"
                ref={register}
              />
            </div>
            <div className="col-12 d-flex justify-content-center">
              <div style={{ textAlign: "center", marginTop: "2vh" }}>
                <Button id="send-button" type="submit" color="success">
                  Enviar
                </Button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}
