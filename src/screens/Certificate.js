import React, { Component, Fragment } from "react";
import { Button } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import Info from "../components/modal/info";
import certificateIcon from "../icons/certificate/certificate.png"

class CertificatePage extends Component {
    constructor(props) {
        super(props);

        this.state = {
            certificate: null,
        };
    }

    componentWillMount = () => {
        API.defaults.headers.common = {
            Authorization: `Bearer ${localStorage.getItem("token")}`,
        };

        this.getCertificate();
    };

    getCertificate = async () => {
        const hash = await this.props.match.params.hash;
        API.get("certificate/" + hash)
            .then((response) => {
                if (response.status === 200) {
                    console.log(response.data)
                    this.setState({ certificate: response.data });
                } else {
                    alert("Ocorreu um erro.");
                }
            })
            .catch((error) => { });
    };

    render() {
        return (
            <div className="row mt-5">
                <h3 className="col-12">Certificado</h3>
                <div className="col-12 d-flex justify-content-center">
                    <img src={certificateIcon} alt="Certificado" style={{ width: "400px" }} />
                </div>
                <div className="col-12 mt-5 d-flex justify-content-center">
                    <div className="col-10 text-left">
                        <p><b>cód.</b> {this.state.certificate?.hash}</p>
                        <p><b>{this.state.certificate?.user?.username}</b></p>
                        <p><b>{this.state.certificate?.course?.name} - versão.</b> {this.state.certificate?.course?.version}</p>
                    </div>
                </div>
            </div>
        );
    }
}

export default CertificatePage;
