import React, { Component, Fragment } from "react";
import { Table, Button, Alert } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import CollegeIcon from "../icons/college.png";

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      college: "",
      list: [],
      error: "",
      message: "",
    };
  }

  componentDidMount = () => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.getMainColleges();
  };

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  getMainColleges = () => {
    API.post("/college/main")
      .then((response) => {
        this.setState({
          list: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleSearch = () => {
    API.post("/college/search", { searchQuery: this.state.college })
      .then((response) => {
        this.setState({
          list: response.data,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.handleSearch();
    }
  };

  enterCollege = (collegeId) => {
    console.log("entrou");
    const errorMessage = "Parece que ocorreu um erro na requisição.";
    API.post("/user/insert/college", { collegeId })
      .then((response) => {
        if (response.status === 200) {
          this.setState({ message: "Faculdade selecionada com sucesso!" });
        } else {
          this.setState({ error: errorMessage });
        }
      })
      .catch((error) => {
        this.setState({ error: errorMessage });
      });
  };

  render() {
    return (
      <Fragment>
        {!this.state.message ? (
          <div onKeyUp={this.handleKeyPress}>
            <div className="d-flex justify-content-center align-items-center mt-5 mb-5">
              <h2>Busque a sua Faculdade</h2>
            </div>
            <div className="mr-5 ml-5">
              <input
                type="text"
                placeholder="Digite para pesquisar"
                onChange={this.handleChange}
                value={this.state.search}
                name="college"
              />
            </div>
            <div className="mt-5">
              <Table>
                <thead>
                  <tr>
                    <th>Nome</th>
                    <th>Cidade</th>
                    <th>Ação</th>
                  </tr>
                </thead>
                <tbody>
                  {this.state.list.map((college) => {
                    return (
                      <tr>
                        <td>{college.name}</td>
                        <td>{college.city}</td>
                        <td>
                          <Button
                            color="success"
                            onClick={() => this.enterCollege(college.id)}
                          >
                            Entrar
                          </Button>
                        </td>
                      </tr>
                    );
                  })}
                </tbody>
              </Table>
            </div>
          </div>
        ) : (
          ""
        )}
        <div className="d-flex ml-5 mr-5 text-center justify-content-center mt-5">
          {this.state.error ? (
            <Alert color="danger">{this.state.error}</Alert>
          ) : (
            ""
          )}
          {this.state.message ? (
            <div>
              <img src={CollegeIcon} alt="Faculdade" />
              <Alert className="mt-4" color="success">
                {this.state.message}
              </Alert>
            </div>
          ) : (
            ""
          )}
        </div>
      </Fragment>
    );
  }
}

export default SignUp;
