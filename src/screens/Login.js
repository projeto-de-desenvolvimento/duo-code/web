import React, { Component } from "react";
import { Button } from "reactstrap";
import "../styles/login.css";
import API from "../api";
import ErrorMessage from "../components/auth/errorMessage";
import brFlagIcon from "../icons/br-flag.png";
import usFlagIcon from "../icons/us-flag.png";
import logo from "../icons/logo.png";
import { Redirect, Link } from "react-router-dom";
import setLang from "../languages/setLang";
import getLang from "../languages/getLang";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      logged: false,
      error: null,
      language_selected: getLang("pt-br"),
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    localStorage.removeItem("token");
  }

  componentDidMount = () => {
    const lang = window.localStorage.getItem("language");

    if (lang) {
      const file = getLang(lang);
      this.setState({ language_selected: file });
    }
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value, error: null });
  };

  handleSubmit = async (event) => {
    event.preventDefault();
    const errors = await this.validation();

    if (errors.length > 0) {
      this.setState({ error: errors });
      return;
    }

    API.post("/auth/login", {
      email: this.state.email,
      password: this.state.password,
    })
      .then(async (response) => {
        if (response.status === 200) {
          const token = await response.data.token;
          if (token) {
            localStorage.setItem("token", token);
            setTimeout(() => this.setState({ logged: true }), 1000);
          } else {
            this.setState({
              error: [this.state.language_selected.login["The server did not respond with the passkey"]],
            });
          }
        } else {
          this.setState({ error: [this.state.language_selected.login["Authentication issue"]] });
        }
      })
      .catch((error) => {
        if (!error.response) {
          this.setState({ error: [this.state.language_selected.login["Server not found"]] });
        } else {
          this.setState({ error: [this.state.language_selected.login["Wrong email or password"]] });
        }
      });
  };

  validation = async () => {
    let errors = [];
    let email = this.state.email;
    let password = this.state.password;
    let emailRegex = new RegExp("^[a-zA-Z][a-zA-Z0-9]+@[a-zA-Z]+.[a-zA-Z]+$");

    if (!emailRegex.test(email)) {
      errors.push(this.state.language_selected.login["You must enter a valid email address."]);
    }

    if (password.length < 6) {
      errors.push(this.state.language_selected.login["You must enter a valid password."]);
    }

    return errors;
  };

  handleKeyPress = (event) => {
    if (event.key === "Enter") {
      this.handleSubmit(event);
    }
  };

  render() {
    return (
      <div className="container" onKeyUp={this.handleKeyPress}>
        <div className="row align-items-center login-panel">
          <div className="background-image"></div>
          <div className="col-sm-12 text-center">
            <img src={logo} alt="logo" id="logoImage" />
            <h3>DuoCode</h3>
            <div className="row d-flex justify-content-center align-items-center">
              <div className="col-sm-8 px-5">
                <div className="row d-flex justify-content-center">
                  <div className="col-sm-8" id="border">
                    <div id="form">
                      <label for="email">
                        {this.state.language_selected.login.mail}
                        <br />
                        <input
                          type="text"
                          id="email"
                          name="email"
                          value={this.state.email}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                      <br />
                      <label for="password">
                        {this.state.language_selected.login.password}
                        <br />
                        <input
                          type="password"
                          id="password"
                          name="password"
                          value={this.state.password}
                          onChange={this.handleChange}
                        ></input>
                      </label>
                    </div>
                  </div>
                </div>
              </div>

              <div className="col-sm-12">
                <div style={{ textAlign: "center", marginTop: "1vh" }}>
                  <span>
                    {this.state.language_selected.login["Don't have an account?"]} <a href="/register">{this.state.language_selected.login["Create Now!"]}</a>
                  </span>
                </div>
                <div className="col-12 d-flex justify-content-center">
                  {this.state.error ? (
                    <ErrorMessage error={this.state.error} />
                  ) : (
                      ""
                    )}
                </div>
                <form onSubmit={this.handleSubmit}>
                  <div style={{ textAlign: "center", marginTop: "2vh" }}>
                    <Button type="submit" color="success">
                      {this.state.language_selected.login["log in"]}
                    </Button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        {this.state.logged ? <Redirect to="/" /> : ""}
        <div className="row">
          <div className="col-sm-12 d-flex justify-content-center" id="login-footer">
            <div>
              <Button color="link" onClick={() => this.setState({ language_selected: setLang("pt-br") })}><img src={brFlagIcon} alt="bandeira" style={{ width: "50px" }} /></Button>
              <Button color="link" onClick={() => this.setState({ language_selected: setLang("en") })}><img src={usFlagIcon} alt="bandeira" style={{ width: "45px" }} /></Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Login;
