import React, { Component, Fragment } from "react";
import { Button } from "reactstrap";
import API from "../api";
import "../styles/coursePage.css";
import Info from "../components/modal/info";
import CourseCard from "../components/CourseCard";

class CoursePage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      course: "",
      newCourseOk: false,
      message: "",
      courses: [],
    };
  }

  componentDidMount = async () => {
    await this.getCourse();
    this.loadCourses();
  };

  loadCourses = () => {
    API.get("/course/get").then((response) => {
      const courses = response.data.filter((c) => c.name != this.state.course.name);
      this.setState({ courses: courses });
    });
  };

  newCourseStudent = async () => {
    API.post("course/user/new", { courseId: this.state.course.id })
      .then((response) => {
        if (response.status === 200) {
          this.setState({
            newCourseOk: true,
            message: "Registrado! Bem vindo ao curso!",
          });
        } else {
          this.setState({
            message: "Você já está matriculado.",
          });
        }
      })
      .catch((error) => {
        this.setState({
          message: "Você já está matriculado.",
        });
      });
  };

  getCourse = async () => {
    const id = await this.props.match.params.id;
    console.log(id);
    await API.get("course/get/" + id)
      .then((response) => {
        if (response.status === 200) {
          this.setState({ course: response.data });
        } else {
          alert("Ocorreu um erro.");
        }
      })
      .catch((error) => { });
  };

  render() {
    return (
      <div>
        <div className="row course-page-board d-flex justify-content-center mt-5">
          <div className="col-12 col-md-3">
            <img
              className="logo"
              src={"" + this.state.course.logoImage}
              alt="Curso"
            />
            <Button
              className="courseStartButton"
              color="success"
              onClick={this.newCourseStudent}
            >
              Começar o Curso
          </Button>
          </div>
          <div className="col-12 col-md-9 ">
            <Info message={this.state.message} />
            <div className="col-12 d-flex justify-content-center">
              <h1>{this.state.course.name}</h1>
            </div>
            <div className="course-text mt-5">
              <p>
                {this.state.course.name == "Javascript" ? "JavaScript (frequentemente abreviado como JS) é uma linguagem de programação interpretada estruturada, de script em alto nível com tipagem dinâmica fraca e multiparadigma (protótipos, orientado a objeto, imperativo e, funcional). Juntamente com HTML e CSS, o JavaScript é uma das três principais tecnologias da World Wide Web. JavaScript permite páginas da Web interativas e, portanto, é uma parte essencial dos aplicativos da web. A grande maioria dos sites usa, e todos os principais navegadores têm um mecanismo JavaScript dedicado para executá-lo."
                  :
                  "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
                }
              </p>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-12 float-left">
            <h2>Explorar</h2>
            <div
              className="container-fluid md-12"
              style={{ backgroundColor: "white" }}
            >
              <br />
              <div className="row card-columns col-sm-12">
                {this.state.courses.map((course) => (
                  <CourseCard
                    key={course.id}
                    id={course.id}
                    name={course.name}
                    logoImage={course.logoImage}
                  />
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CoursePage;
