import React, { Component, Fragment } from "react";
import { Redirect } from "react-router-dom";
import "../styles/main.css";

import CourseCard from "../components/CourseCard";
import LearnCard from "../components/LearnCard";
import API from "../api";
import ChooseCourse from "../components/popup/chooseCourse";
import ChooseCollege from "../components/popup/chooseCollege";
const Promise = require('promise');

class Main extends Component {
  state = {
    fire: 0,
    courses: [],
    myCourses: [],
    noLogged: false,
    registeredCourses: undefined,
    college: undefined,
    redirectSelectCollege: false,
  };

  componentDidMount = async () => {
    API.defaults.headers.common = {
      Authorization: `Bearer ${localStorage.getItem("token")}`,
    };
    this.setState({ noLogged: false });
    await this.loadUserCourses();
    this.loadCourses();
    this.getUserStats();
  };

  getUserStats() {
    API.get("/user/stats")
      .then((response) => {
        const data = response.data;
        this.setState({
          registeredCourses: data.numberOfCourses,
          college: data.college,
        });
      })
      .catch((error) => {
        console.log(error);
      });
  }

  loadCourses = () => {
    API.get("/course/get").then(async (response) => {
      let userCourses = "";
      console.log(this.state.myCourses)
      await Promise.all(this.state.myCourses.map((c) => userCourses = userCourses + c.course.name));
      console.log(userCourses)
      const courses = response.data.filter((c) => !userCourses.includes(c.name));
      this.setState({ courses: courses });
    });
  };

  loadUserCourses = async () => {
    await API.get("/course/user/courses")
      .then((response) => {
        this.setState({ myCourses: response.data });
        console.log(response.status);
      })
      .catch((error) => {
        this.setState({ noLogged: true });
      });
  };

  closeCollegePopup = () => {
    this.setState({ college: -1 });
  };

  enterSelectCollege = () => {
    this.setState({ redirectSelectCollege: true });
  };

  render() {
    return (
      <div className="row">
        {this.state.registeredCourses === 0 ? <ChooseCourse /> : ""}
        {this.state.college === 0 && this.state.registeredCourses ? (
          <ChooseCollege
            closePopup={() => this.closeCollegePopup()}
            enterSelectCollege={() => this.enterSelectCollege()}
          />
        ) : (
            ""
          )}
        <div className="col-12 main-board">
          <div className="row">
            <div className="col-12 mt-4 float-left">
              {this.state.myCourses.length == 0 ? "" : <h2>Meus Cursos</h2>}
              <div
                className="container-fluid md-12"
                style={{ backgroundColor: "white" }}
              >
                <br />
                <div className="row card-columns col-sm-12">
                  {this.state.myCourses.map((my) => (
                    <LearnCard
                      key={my.course.id}
                      id={my.course.id}
                      name={my.course.name}
                      logoImage={my.course.logoImage}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-12 float-left">
              <h2>Explorar</h2>
              <div
                className="container-fluid md-12"
                style={{ backgroundColor: "white" }}
              >
                <br />
                <div className="row card-columns col-sm-12">
                  {this.state.courses.map((course) => (
                    <CourseCard
                      key={course.id}
                      id={course.id}
                      name={course.name}
                      logoImage={course.logoImage}
                    />
                  ))}
                </div>
              </div>
            </div>
          </div>
          {this.state.noLogged ? <Redirect to="/login" /> : ""}
          {this.state.redirectSelectCollege ? (
            <Redirect to="/selectCollege" />
          ) : (
              ""
            )}
        </div>
      </div>
    );
  }
}

export default Main;
