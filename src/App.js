import React, { Component } from 'react';
import './App.css';

import 'bootstrap/dist/js/bootstrap.bundle.min';

import Routes from './routes'

const App = () => <Routes />

export default App;
