import React from "react";
import { isAuthenticated } from "./auth";
import {
  BrowserRouter,
  Route,
  Switch,
  Redirect,
  withRouter,
} from "react-router-dom";
import Login from "./screens/Login";
import Main from "./screens/Main";
import SignUp from "./screens/SignUp";
import CoursePage from "./screens/CoursePage";
import Menu from "./screens/LayoutMenu";
import LearnTree from "./screens/LearnTree";
import Session from "./screens/Session";
import SelectCollege from "./screens/SelectCollege";
import Ranking from "./screens/ranking";
import Community from "./screens/Community";
import NewQuestion from "./screens/NewQuestion";
import QuestionPage from "./screens/QuestionPage";
import RankingCollegeStudents from "./screens/ranking_students";
import Profile from "./screens/Profile";
import CertificatePage from "./screens/Certificate";

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) =>
      isAuthenticated() ? (
        <Component {...props} />
      ) : (
          <Redirect
            to={{ pathname: "/login", state: { from: props.location } }}
          />
        )
    }
  />
);

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route path="/login" component={Login} />
      <Route path="/register" component={SignUp} />
      <PrivateRoute
        path="/learn/:courseId/session/:moduleId"
        component={Session}
      />
      <Menu>
        <PrivateRoute path="/" exact component={Main} />
        <PrivateRoute path="/course/:id" component={CoursePage} />
        <PrivateRoute path="/learn/:id" component={LearnTree} />
        <PrivateRoute path="/selectCollege" exact component={SelectCollege} />
        <PrivateRoute path="/ranking" exact component={Ranking} />
        <PrivateRoute
          path="/ranking/students"
          exact
          component={RankingCollegeStudents}
        />
        <PrivateRoute path="/community" exact component={Community} />
        <PrivateRoute path="/community/newQuestion" component={NewQuestion} />
        <PrivateRoute path="/community/question/:id" component={QuestionPage} />
        <PrivateRoute path="/profile" component={Profile} />
        <PrivateRoute path="/certificate/:hash" component={CertificatePage} />
      </Menu>
    </Switch>
  </BrowserRouter>
);

export default Routes;
