import React from "react";
import "../../styles/popup/chooseCourse.css";
import binoculars from "../../icons/popup/binoculars.png";

const ChooseCourse = (props) => (
  <div className="col-12 popup d-flex justify-content-center">
    <div className="popup-container col-11 d-flex align-items-center">
      <img src={binoculars} alt="Binóculo" className="popup-icon" />
      <span>Está na hora de escolher um curso!</span>
    </div>
  </div>
);

export default ChooseCourse;
