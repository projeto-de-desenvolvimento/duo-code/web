import React from "react";
import { Button } from "reactstrap";
import "../../styles/popup/chooseCollege.css";
import award from "../../icons/popup/award.png";

const ChooseCollege = (props) => (
  <div className="col-12 popup d-flex justify-content-center">
    <div className="popup-College-container col-11 d-flex align-items-center">
      <div className="col-2">
        <img src={award} alt="Troféu" className="popup-icon" />
      </div>
      <div className="col-lg-8">
        <span>Selecione sua Faculdade e comece a competir!</span>
      </div>
      <div className="col-2">
        <Button color="success" onClick={props.enterSelectCollege}>
          Inscrever-se
        </Button>
        <Button color="danger" onClick={props.closePopup}>
          Ignorar
        </Button>
      </div>
    </div>
  </div>
);

export default ChooseCollege;
