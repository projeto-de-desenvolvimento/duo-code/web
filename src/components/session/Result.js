import React from "react";
import TrophyIcon from "../../icons/trophy.png";
import { Button } from "reactstrap";

const result = (props) => {
  return (
    <div
      className="row d-flex align-items-center"
      style={{ marginTop: "20vh" }}
    >
      <div
        className="col-12 d-flex justify-content-center align-items-center"
        style={{ height: "30vh" }}
      >
        <img
          src={TrophyIcon}
          alt="logo"
          id="courseImage"
          style={{ width: "200px" }}
        />
      </div>
      <div className="col-12">
        <h4>Você ganhou {props.result_totalXp} XP!</h4>
      </div>
      <div className="col-12">
        {props.result_newLevel ? <h3>Você avançou de nível!</h3> : ""}
      </div>
      <div className="col-12 d-flex justify-content-center align-items-center">
        <Button color="success" onClick={() => props.exitSession()}>
          Finalizar
        </Button>
      </div>
    </div>
  );
};

export default result;
