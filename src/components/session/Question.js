import React, { Component } from "react";
import { Spinner, Row, Container, Col, Button } from "reactstrap";
import Introduction from "./questionFormat/Introduction";

export default class Question extends Component {
  constructor(props) {
    super(props);
    this.state = {
      options: [],
      answer: "",
    };
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };

  componentDidMount() { }

  sendVerifyQuestion(opt) {
    this.props.verifyQuestion(opt);
  }

  randomInt = (min, max) => {
    return min + Math.floor((max - min) * Math.random());
  };

  getOptions = () => {
    let junto;
    let options = this.props.actualQuestion.answer.split("*");
    let sortedOptions = [];
    while (options.length > 0) {
      let x = this.randomInt(0, options.length);
      sortedOptions.push(options[x]);
      options.splice(x, 1);
    }

    return sortedOptions.map((opt) => {
      return (
        <Row>
          <Col className="d-flex justify-content-center">
            <div
              id="optionButton"
              onClick={() => {
                this.props.verifyQuestion(opt);
                this.setState({ clicked: true });
              }}
            >
              {opt}
            </div>
          </Col>
        </Row>
      );
    });
  };

  render() {
    {
      if (this.props.actualQuestion.format === 0) {
        return (
          <Introduction
            actualQuestion={this.props.actualQuestion}
            nextQuestion={this.props.nextQuestion}
          />
        );
      } else if (this.props.actualQuestion.format === 1) {
        return (
          <Container>
            <Row>
              <Col className="d-flex justify-content-center">
                <div id="questionContainer" style={{ heigth: "20vh" }}>
                  <h4>{this.props.actualQuestion.questionText}</h4>
                </div>
              </Col>
            </Row>
            <Container>{this.getOptions()}</Container>
          </Container>
        );
      } else if (this.props.actualQuestion.format === 2) {
        return (
          <Container>
            <Row>
              <Col className="d-flex justify-content-center">
                <div id="questionContainer" style={{ heigth: "20vh" }}>
                  <h4>{this.props.actualQuestion.questionText}</h4>
                </div>
              </Col>
            </Row>
            <div className="d-flex justify-content-center">
              <Button
                color="success"
                onClick={() => {
                  this.props.verifyQuestion("true");
                  this.setState({ clicked: true });
                }}
                className="mr-3"
                style={{ width: "130px" }}
              >
                Verdadeiro
              </Button>
              <Button
                color="danger"
                onClick={() => {
                  this.props.verifyQuestion("false");
                  this.setState({ clicked: true });
                }}
                style={{ width: "130px" }}
              >
                Falso
              </Button>
            </div>
          </Container>
        );
      } else if (this.props.actualQuestion.format === 3) {
        return (
          <Container className="question-write d-flex align-items-center">
            <div>
              <Row>
                <Col className="d-flex justify-content-center">
                  <div className="row" style={{ heigth: "20vh" }}>
                    <div className="col-12">
                      <h4>
                        {this.props.actualQuestion.questionText.split("/")[0]}
                      </h4>
                    </div>
                    <div className="col-12 d-flex justify-content-center mt-1">
                      <div className="text-left">
                        {this.props.actualQuestion.questionText.split("/").pop().split("<br>").map((i) => {
                          if (i.includes("<tab1>")) {
                            return <p style={{ marginLeft: "30px" }}>{i.replace("<tab1>", "")}</p>
                          } else if (i.includes("<tab2>")) {
                            return <p style={{ marginLeft: "60px" }}>{i.replace("<tab2>", "")}</p>
                          } else if (i.includes("<tab3>")) {
                            return <p style={{ marginLeft: "90px" }}>{i.replace("<tab3>", "")}</p>
                          } else if (i.includes("<tab4>")) {
                            return <p style={{ marginLeft: "120px" }}>{i.replace("<tab4>", "")}</p>
                          } else {
                            return <p>{i}</p>
                          }
                        })}
                      </div>
                    </div>
                  </div>
                </Col>
              </Row>
              <div className="row mt-5 d-flex justify-content-center">
                <div className="col-6 answer-input text-center">
                  <input
                    type="text"
                    id="answer"
                    name="answer"
                    value={this.state.answer}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="col-12 mt-5 d-flex justify-content-center">
                  <Button
                    onClick={() => {
                      this.props.verifyQuestion(this.state.answer);
                      this.setState({ clicked: true });
                    }}
                    style={{ width: "130px" }}
                    color="success"
                  >
                    Enviar
                  </Button>
                </div>
              </div>
            </div>
          </Container>
        );
      }
      return <Spinner></Spinner>;
    }
  }
}
