import React from "react";
import { Row, Container, Col } from "reactstrap";
import IconIdea from "../../../icons/idea.png";

const introduction = (props) => {
  return (
    <Container>
      <Row>
        <Col className="d-flex justify-content-center">
          <div id="infoContainer">
            <h3 id="infoTitle">{props.actualQuestion.title}</h3>
            <div className="">
              <img src={IconIdea} alt="" id="iconIdea" />
            </div>
            <div className="text-left ml-5 mt-5" style={{ marginTop: "10px" }}>
              {/* {props.actualQuestion.questionText.split("<br>").maps} */}
              {props.actualQuestion.questionText.split("<br>").map((i) => {
                if (i.includes("<tab1>")) {
                  return <p style={{ marginLeft: "30px" }}>{i.replace("<tab1>", "")}</p>
                } else if (i.includes("<tab2>")) {
                  return <p style={{ marginLeft: "60px" }}>{i.replace("<tab2>", "")}</p>
                } else if (i.includes("<tab3>")) {
                  return <p style={{ marginLeft: "90px" }}>{i.replace("<tab3>", "")}</p>
                } else if (i.includes("<tab4>")) {
                  return <p style={{ marginLeft: "120px" }}>{i.replace("<tab4>", "")}</p>
                } else {
                  return <p>{i}</p>
                }
              })}
            </div>
          </div>
        </Col>
      </Row>
      <Row>
        <Col className="d-flex justify-content-center mt-4">
          <div id="infoButton" onClick={props.nextQuestion}>
            Iniciar
          </div>
        </Col>
      </Row>
    </Container>
  );
};

export default introduction;
