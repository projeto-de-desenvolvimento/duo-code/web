import React from "react";
import "../../styles/community/communityMain.css";
import profileImage from "../../icons/person.png";
import commentIcon from "../../icons/community/comment.png";
import { Link } from "react-router-dom";
import TagsComponent from "./tags";

const askingPanel = (props) => (
  <Link to={`/community/question/${props.ask.id}`} style={{ color: "black" }}>
    <div className="row asking-panel my-2">
      <div className="col-2 d-flex justify-content-center align-items-center">
        <img
          src={profileImage}
          alt="Perfil"
          className="profileImage"
          style={{ width: "70px" }}
        />
      </div>
      <div className="col-10">
        <div className="d-inline-block">
          <h4>{props.ask.title.substring(0, 45)}</h4>
        </div>
        <div className="mt-1 number-comments">
          <div className="d-inline-block mr-1">
            <img src={commentIcon} alt="Comentário" style={{ width: "20px" }} />
          </div>
          <div className="d-inline-block">
            <p className="comments-count">{props.ask.number_comments}</p>
          </div>
        </div>
        <h6>{props.ask.content.substring(0, 70)}</h6>
        <div className="">
          <TagsComponent tags={props.ask.tags} />
        </div>
      </div>
    </div>
  </Link>
);

export default askingPanel;
