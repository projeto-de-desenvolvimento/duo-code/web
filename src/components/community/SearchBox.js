import React from "react";
import { Input } from "reactstrap";
import searchIcon from "../../icons/community/search.png";

const SearchBox = (props) => (
  <div>
    <div className="d-inline-block">
      <img className="" src={searchIcon} alt="" style={{ width: "30px" }} />
    </div>
    <div className="d-inline-block mx-3">
      <Input className="" placeholder="Pesquisa" />
    </div>
  </div>
);

export default SearchBox;
