import React from "react";
import { Button, Col, Row } from "reactstrap";

const ErrorTip = (props) => {
  return (
    <Row className="d-flex justify-content-center mt-5">
      <div className="col-12 d-flex justify-content-center">
        <h2 style={{ fontSize: "20px" }}>{props.errorTip}</h2>
      </div>
      <div className="col-12 mt-3 d-flex justify-content-center">
        <Button color="info" onClick={() => props.nextQuestion()}>
          Continuar
        </Button>
      </div>
    </Row>
  );
};

export default ErrorTip;
