import React from 'react'
import '../styles/footer.css'
import br_flag from '../icons/br-flag.png'
import us_flag from '../icons/us-flag.png'
import twitterIcon from '../icons/twitter.png'
import facebookIcon from '../icons/facebook.png'

const Footer = () => {
    return (
        <div className="main-footer d-flex">
            <div className="col-4 social-icons d-flex align-items-center">
                <div className="mr-3">
                    <img src={facebookIcon} alt="link para o facebook" />
                </div>
                <div>
                    <img src={twitterIcon} alt="link para o twitter" />
                </div>
            </div>
            <div className="col-4 text d-flex">
                <div className="row">
                    <div className="col-12 flags d-flex mt-2 justify-content-center">
                        <div className="mr-3">
                            <img src={br_flag} alt="Bandeira brasileira" />
                        </div>
                        <div>
                            <img src={us_flag} alt="Bandeira USA" />
                        </div>
                    </div>
                    <div className="col-12 d-flex justify-content-center align-items-end mb-1">
                        <span>© 2020 DuoCode, Todos os direitos reservados</span>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Footer

