import React from "react";
import errorIcon from "../../icons/auth/warn.png";

const errorMessage = (props) => {
  return (
    <div className="d-flex align-items-center alert-danger rounded mt-3 text-left">
      <img
        src={errorIcon}
        alt="Erro"
        style={{
          width: "40px",
          margin: "5px",
          marginRight: "15px",
        }}
        className="d-inline-block ml-3"
      />
      <pre className="d-inline-block mr-3">
        {props.error.map((err) => {
          return `. ${err}\n`;
        })}
      </pre>
    </div>
  );
};

export default errorMessage;
